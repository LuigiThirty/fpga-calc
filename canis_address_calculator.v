module canis_address_calculator
(
  input update,

  input [15:0] address,

  input [15:0] rSP,

  // index registers
  input [7:0] rX,
  input [7:0] rY,

  output reg [15:0] calculated_address_x,
  output reg [15:0] calculated_address_y,

  output reg [15:0] address_plus_one,

  output reg [15:0] sp_plus_one,
  output reg [15:0] sp_minus_one,
  output reg [15:0] sp_plus_two,
  output reg [15:0] sp_minus_two
);

always @(negedge update)
begin
  calculated_address_x = address + rX;
  calculated_address_y = address + rY;
  address_plus_one = address + 1;
  sp_plus_one = rSP + 1;
  sp_plus_two = rSP + 2;
  sp_minus_one = rSP - 1;
  sp_minus_two = rSP - 2;
end

endmodule
