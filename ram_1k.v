module ram_1k (
  input clock,
  input wen,
  input [9:0] addr,

  input [7:0] data_bus_in,
  output [7:0] data_bus_out,

  output [7:0] debug_out,

  input cs
);

wire work_ram0_chip_select, work_ram1_chip_select, work_ram2_chip_select, work_ram3_chip_select;

assign work_ram0_chip_select = cs && (addr[9:8] == 0) ? 1 : 0;
assign work_ram1_chip_select = cs && (addr[9:8] == 1) ? 1 : 0;
assign work_ram2_chip_select = cs && (addr[9:8] == 2) ? 1 : 0;
assign work_ram3_chip_select = cs && (addr[9:8] == 3) ? 1 : 0;

// 4:1 RAM output mux
wire [7:0] ram_mux_0, ram_mux_1, ram_mux_2, ram_mux_3;
wire [7:0] ram_mux_out;
reg [1:0] ram_mux_selector;
four_to_one_mux #(.BUS_WIDTH(8)) ram_output_mux(
  .a(ram_mux_0),
  .b(ram_mux_1),
  .c(ram_mux_2),
  .d(ram_mux_3),
  .out(data_bus_out),
  .select(ram_mux_selector)
  );

work_ram work_ram_0
(
  .rclock(~clock),
  .wclock(clock),

  .wen(wen && work_ram0_chip_select),
  .ren(cs && work_ram0_chip_select),

  .waddr(addr[7:0]),
  .raddr(addr[7:0]),

  .data_bus_in(data_bus_in),
  .data_bus_out(ram_mux_0)
);

work_ram work_ram_1
(
  .rclock(~clock),
  .wclock(clock),

  .wen(wen && work_ram1_chip_select),
  .ren(cs && work_ram1_chip_select),

  .waddr(addr[7:0]),
  .raddr(addr[7:0]),

  .data_bus_in(data_bus_in),
  .data_bus_out(ram_mux_1)
);

work_ram work_ram_2
(
  .rclock(~clock),
  .wclock(clock),

  .wen(wen && work_ram2_chip_select),
  .ren(cs && work_ram2_chip_select),

  .waddr(addr[7:0]),
  .raddr(addr[7:0]),

  .data_bus_in(data_bus_in),
  .data_bus_out(ram_mux_2)
);

work_ram work_ram_3
(
  .rclock(~clock),
  .wclock(clock),

  .wen(wen && work_ram3_chip_select),
  .ren(cs && work_ram3_chip_select),

  .waddr(addr[7:0]),
  .raddr(addr[7:0]),

  .data_bus_in(data_bus_in),
  .data_bus_out(ram_mux_3)
);

always @(negedge clock) begin
  if(work_ram0_chip_select) ram_mux_selector <= 0;
  else if(work_ram1_chip_select) ram_mux_selector <= 1;
  else if(work_ram2_chip_select) ram_mux_selector <= 2;
  else if(work_ram3_chip_select) ram_mux_selector <= 3;
end

endmodule
