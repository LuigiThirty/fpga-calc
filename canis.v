`include "includes/cpudefines.vh"

module canis
(
  input clock,
  output [15:0] address_bus,

  input [7:0] data_bus_in,
  output reg [7:0] data_bus_out,

  input reset,
  input nmi,

  output reg write_operation                  // high if this is a write operation
);

// Condition flags
reg flag_negative = 0;
reg flag_overflow = 0;
reg flag_carry = 0;
reg flag_zero = 0;
reg flag_interrupts_off = 0;
wire [7:0] condition_flags;

assign condition_flags[7] = flag_negative;
assign condition_flags[6] = flag_overflow;
assign condition_flags[5] = flag_carry;
assign condition_flags[4] = flag_zero;
assign condition_flags[3] = flag_interrupts_off;
assign condition_flags[2] = 0;
assign condition_flags[1] = 0;
assign condition_flags[0] = 0;


/* Tasks */
task advance_pc;
    pc <= pc + 1;
endtask

task update_cflags_zn(input reg [7:0] Q);
  begin
    flag_zero <= (Q == 0);
    flag_negative <= Q[7];
  end
endtask

reg reset_to_state_0_at_end_of_cycle;
task reset_to_state_0;
  begin
    cpu_state <= 0;
    clocks_in_this_state <= 0;
    ireg <= 0;
    advance_state <= 0;
    write_operation <= 0;
    alu_GO <= 0;
    alu_use_control_lines <= 0;
    register_file_write <= 0;
    reset_to_state_0_at_end_of_cycle <= 0;
  end
endtask

`include "canis_execute.vh"

reg [7:0] obt_opcode;
wire [1:0] obt_operand_byte_count;
canis_opcode_byte_table opcode_byte_table
(
  .opcode(obt_opcode),
  .operand_byte_count(obt_operand_byte_count)
);

reg [3:0] alu_operation;
reg [7:0] alu_A, alu_B;
wire [7:0] alu_R;
wire alu_flag_N, alu_flag_V, alu_flag_C, alu_flag_Z;
reg alu_GO;
reg [3:0] alu_ctrl_input_A, alu_ctrl_input_B;
reg alu_use_control_lines;
reg update_flags_from_alu;

canis_alu alu
(
  .clock(clock),
  .operation(alu_operation),
  .go(alu_GO),
  .A(alu_A),
  .B(alu_B),
  .R(alu_R),
  .carry(flag_carry),

  .f_carry(alu_flag_C),
  .f_overflow(alu_flag_V),
  .f_negative(alu_flag_N),
  .f_zero(alu_flag_Z)
);

reg [3:0] clocks_in_this_state;
reg cpu_state;
reg advance_state;

// Registers. SP = stack pointer, SV = System Vectors - page containing SYS vectors
// SA = Sys Args - pointer to location containing syscall arguments
reg [15:0] pc;
reg [15:0] reg_SP;
reg [7:0] reg_SV;

reg [31:0] ireg;

reg temp_bit1;
reg [7:0] temp_value;
reg [15:0] temp16_value;

reg [15:0] address_bus_execution, address_bus_fetch = 0;
reg [1:0] address_bus_mux_selector;

four_to_one_mux #(.BUS_WIDTH(16)) address_bus_source_mux
(
  .a(address_bus_fetch),
  .b(address_bus_execution),
  .c(8'h0),
  .d(8'h0),
  .out(address_bus),
  .select(address_bus_mux_selector)
);

// opcode map
// the high 3 bits define how many bytes the instruction is.
// instruction format is usually:
//   MMM FF AAA
//  where MMM = major mode, FF = minor function, and AAA = addressing mode

reg [2:0] operand_bytes_to_fetch = 0;

/*
  0 A
  1 X
  2 Y
  3 S
  4 PC
  5 condition flags
  6
  7
*/
reg [2:0] minor_mode;

/*
  0 Immediate
  1 Absolute 16
  2 Absolute 8,X
  3 Absolute 8,Y
  4 (Absolute 8),Y
  5 (Absolute 8,X)
  6
  7
*/
reg [3:0] addressing_mode;

reg [2:0] major_mode;
reg [15:0] data;
reg initialized;

initial begin
  initialized = 0;
  clocks_in_this_state = 0;
  cpu_state = 0;
  advance_state = 0;
end

reg [1:0] register_file_port1_select = 0, register_file_port2_select = 0;
reg [7:0] register_file_write_data = 0;
wire [7:0] register_file_port1_output, register_file_port2_output;
reg register_file_write = 0, register_file_reset = 0;

wire [7:0] reg_X, reg_Y;
canis_register_file canis_register_file
(
  .register_port1_select(register_file_port1_select),
  .read_value_port1(register_file_port1_output),

  .register_port2_select(register_file_port2_select),
  .read_value_port2(register_file_port2_output),

  .write(register_file_write),
  .write_data(register_file_write_data),
  .reset(register_file_reset),

  .reg_X_private(reg_X),
  .reg_Y_private(reg_Y)
);

//
wire [15:0] calculated_address_X;
wire [15:0] calculated_address_Y;
wire [15:0] address_plus_one;
wire [15:0] sp_plus_one, sp_minus_one, sp_plus_two, sp_minus_two;
canis_address_calculator address_calculator(
  .update(clock),

  .address(address_bus),

  .rSP(reg_SP),

  .rX(reg_X),
  .rY(reg_Y),

  .calculated_address_x(calculated_address_X),
  .calculated_address_y(calculated_address_Y),
  .address_plus_one(address_plus_one),

  .sp_plus_one(sp_plus_one),
  .sp_plus_two(sp_plus_two),
  .sp_minus_one(sp_minus_one)
);


always @(posedge clock)

begin
  clocks_in_this_state = clocks_in_this_state + 1;

  if(!initialized)
  begin
    case (clocks_in_this_state)
    1:
    begin
      reg_SP <= 16'h0100;
      pc <= 16'hF000;

      ireg <= 0;

      temp_value <= 0;
      temp16_value <= 0;
      alu_GO <= 0;
    end
    2: begin initialized <= 1; clocks_in_this_state <= 0; end
    endcase
  end

  else
  begin

    write_operation <= 0;

    // START FETCH
    case (cpu_state)
    `CPU_STATE_FETCH:
    begin
      address_bus_fetch = pc;

      case (clocks_in_this_state)
      1:
      begin
        address_bus_mux_selector <= `CPUMUX_FETCH;
        operand_bytes_to_fetch <= 1;
      end
      2:
      begin
        ireg[7:0] <= data_bus_in[7:0];
        obt_opcode <= data_bus_in;

        addressing_mode <= data_bus_in[2:0];
        minor_mode <= data_bus_in[4:3];
        major_mode <= data_bus_in[7:5];
      end
      3:
      begin
        advance_pc;
        operand_bytes_to_fetch <= obt_operand_byte_count;
        if(obt_operand_byte_count == 0) advance_state <= 1;
      end
      4: ;
      5:
      begin
        ireg[15:8] <= data_bus_in;
        advance_pc;
        operand_bytes_to_fetch <= operand_bytes_to_fetch - 1;
      end
      6:
      begin
        if(operand_bytes_to_fetch == 0) advance_state = 1;
      end
      7:
      begin
        ireg[23:16] = data_bus_in;
        advance_pc;
        operand_bytes_to_fetch <= operand_bytes_to_fetch - 1;

        // preload
        address_bus_execution = ireg[23:8];
        address_bus_mux_selector <= `CPUMUX_EXECUTE;
      end
      default: advance_state <= 1;
      endcase
    end
      // END Fetch

    // START Execute
    `CPU_STATE_EXECUTE:
    begin

      // If the instruction has a 16-bit address operand, the byte at the address
      // is loaded into data_bus_in on Cycle 2.
      data = ireg[23:8];
      address_bus_mux_selector <= `CPUMUX_EXECUTE;

      // Decode the major mode.
      // Major Mode 0 - implied operands
      case (major_mode)
      0:
      begin
        if(ireg[7:0] == 8'h0F) // RTS
        begin
          case (clocks_in_this_state)
          2: address_bus_execution <= sp_plus_one;
          4: pc[7:0] <= data_bus_in[7:0];
          5: address_bus_execution <= sp_plus_two;
          7: pc[15:8] <= data_bus_in[7:0];
          8: begin reg_SP <= sp_plus_two; reset_to_state_0_at_end_of_cycle = 1; end
          default: ;
          endcase
        end
        else

          case (minor_mode)
          0:
          begin
            case (ireg[3:0])
            0: // BRK
              address_bus_mux_selector <= 3; // detach from address bus, effectively halting the system

            1: // INA
            begin
              case (clocks_in_this_state)
              1:
              begin
                alu_operation <= `ALU_OP_ADD;
                alu_ctrl_input_A <= `ALU_INPUT_REG_A;
                alu_ctrl_input_B <= `ALU_INPUT_ONE;
                register_file_port1_select <= `REGFILE_A;
                alu_use_control_lines <= 1;
              end
              2: alu_GO <= 1;
              3: register_file_write_data <= alu_R;
              4: register_file_write <= 1;
              5: reset_to_state_0_at_end_of_cycle <= 1;
              endcase
            end
            2: // INX
            begin
            case (clocks_in_this_state)
            1:
            begin
              alu_operation <= `ALU_OP_ADD;
              alu_ctrl_input_A <= `ALU_INPUT_REG_X;
              alu_ctrl_input_B <= `ALU_INPUT_ONE;
              register_file_port1_select <= `REGFILE_X;
              alu_use_control_lines <= 1;
            end
            2: alu_GO <= 1;
            3: register_file_write_data <= alu_R;
            4: register_file_write <= 1;
            5: reset_to_state_0_at_end_of_cycle <= 1;
            endcase
            end
            3: // INY
            begin

              case (clocks_in_this_state)
              1:
              begin
                alu_operation <= `ALU_OP_ADD;
                alu_ctrl_input_A <= `ALU_INPUT_REG_Y;
                alu_ctrl_input_B <= `ALU_INPUT_ONE;
                register_file_port1_select <= `REGFILE_Y;
                alu_use_control_lines <= 1;
              end
              2: alu_GO <= 1;
              3: register_file_write_data <= alu_R;
              4: register_file_write <= 1;
              5: reset_to_state_0_at_end_of_cycle <= 1;
              endcase

            end
            4: reset_to_state_0_at_end_of_cycle <= 1; // NOP
            default: ;
            endcase
          end

          1:
          begin
            case (ireg[3:0])
            8: // SEC
            begin
              flag_carry <= 1;
            end
            9: // CLC
            begin
              flag_carry <= 0;
            end
            endcase

            reset_to_state_0_at_end_of_cycle <= 1;
          end

          2:
          begin

            case (ireg[3:0])
            1: // DEA
            begin
              case (clocks_in_this_state)
              1:
              begin
                alu_operation <= `ALU_OP_SUB;
                alu_ctrl_input_A <= `ALU_INPUT_REG_A;
                alu_ctrl_input_B <= `ALU_INPUT_ONE;
                register_file_port1_select <= `REGFILE_A;
                alu_use_control_lines <= 1;
              end
              2: alu_GO <= 1;
              3: register_file_write_data <= alu_R;
              4: register_file_write <= 1;
              5: reset_to_state_0_at_end_of_cycle <= 1;
              endcase
            end
            2: // DEX
            begin
              case (clocks_in_this_state)
              1:
              begin
                alu_operation <= `ALU_OP_SUB;
                alu_ctrl_input_A <= `ALU_INPUT_REG_X;
                alu_ctrl_input_B <= `ALU_INPUT_ONE;
                register_file_port1_select <= `REGFILE_X;
                alu_use_control_lines <= 1;
              end
              2: alu_GO <= 1;
              3: register_file_write_data <= alu_R;
              4: register_file_write <= 1;
              5: reset_to_state_0_at_end_of_cycle <= 1;
              endcase
            end
            3: // DEY
            begin
              case (clocks_in_this_state)
              1:
              begin
                alu_operation <= `ALU_OP_SUB;
                alu_ctrl_input_A <= `ALU_INPUT_REG_Y;
                alu_ctrl_input_B <= `ALU_INPUT_ONE;
                register_file_port1_select <= `REGFILE_Y;
                alu_use_control_lines <= 1;
              end
              2: alu_GO <= 1;
              3: register_file_write_data <= alu_R;
              4: register_file_write <= 1;
              5: reset_to_state_0_at_end_of_cycle <= 1;
              endcase
            end
            endcase

          end

          3:
          begin

            // Register-Register Transfers
            case (ireg[2:0])
            0:
            begin
              case (clocks_in_this_state)
              1: register_file_port1_select <= `REGFILE_A;
              2: temp_value <= register_file_port1_output;
              3: register_file_port1_select <= `REGFILE_X;
              4: register_file_write_data <= temp_value;
              5: register_file_write <= 1;
              6: begin update_cflags_zn(register_file_port1_output); reset_to_state_0_at_end_of_cycle <= 1; end
              endcase
            end
            1:
            begin
            case (clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_A;
            2: temp_value <= register_file_port1_output;
            3: register_file_port1_select <= `REGFILE_Y;
            4: register_file_write_data <= temp_value;
            5: register_file_write <= 1;
            6: begin update_cflags_zn(register_file_port1_output); reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
            end
            2:
            begin
            case (clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_X;
            2: temp_value <= register_file_port1_output;
            3: register_file_port1_select <= `REGFILE_A;
            4: register_file_write_data <= temp_value;
            5: register_file_write <= 1;
            6: begin update_cflags_zn(register_file_port1_output); reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
            end
            3:
            begin
            case (clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_Y;
            2: temp_value <= register_file_port1_output;
            3: register_file_port1_select <= `REGFILE_A;
            4: register_file_write_data <= temp_value;
            5: register_file_write <= 1;
            6: begin update_cflags_zn(register_file_port1_output); reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
            end
            4:
            begin
              case (clocks_in_this_state)
              1: register_file_port1_select <= `REGFILE_A;
              2: reg_SP <= register_file_port1_output;
              3: begin update_cflags_zn(reg_SP); reset_to_state_0_at_end_of_cycle <= 1; end
              endcase
            end
            5:
            begin
              case (clocks_in_this_state)
              1: register_file_port1_select <= `REGFILE_A;
              2: register_file_write_data <= reg_SP[7:0];
              3: register_file_write <= 1;
              4: begin update_cflags_zn(reg_SP); reset_to_state_0_at_end_of_cycle <= 1; end
              endcase
            end
            endcase

          end
          endcase
      end
      // Major Mode 1
      1:
      begin
        if(minor_mode != 2)
        begin

          case (addressing_mode)
          0:
          begin

            case(clocks_in_this_state)
            1:
              begin
              case (minor_mode)
                0: begin register_file_port1_select <= `REGFILE_A; register_file_write_data <= data[7:0]; end
                1: begin register_file_port1_select <= `REGFILE_X; register_file_write_data <= data[7:0]; end
                3: begin register_file_port1_select <= `REGFILE_Y; register_file_write_data <= data[7:0]; end
                default: ;
              endcase

              flag_zero <= data[7:0] == 0;
              flag_negative <= data[7];
              end
            2: register_file_write <= 1;
            3: reset_to_state_0_at_end_of_cycle <= 1;
          endcase
          end

          1:
          begin
            case (clocks_in_this_state)
            2:
            begin
              case (minor_mode)
                0: begin register_file_port1_select <= `REGFILE_A; register_file_write_data <= data_bus_in; end
                1: begin register_file_port1_select <= `REGFILE_X; register_file_write_data <= data_bus_in; end
                3: begin register_file_port1_select <= `REGFILE_Y; register_file_write_data <= data_bus_in; end
                default: ;
              endcase

              flag_zero <= data_bus_in == 0;
              flag_negative <= data_bus_in[7];
            end
            3: register_file_write <= 1;
            4: reset_to_state_0_at_end_of_cycle = 1;
            default: ;
            endcase
          end

          2:
          begin
            case (clocks_in_this_state)
            1: address_bus_execution <= calculated_address_X;
            3:
            begin
              case (minor_mode)
                0: begin register_file_port1_select <= `REGFILE_A; register_file_write_data <= data_bus_in; end
                3: begin register_file_port1_select <= `REGFILE_Y; register_file_write_data <= data_bus_in; end
                default: ;
              endcase

              flag_zero <= data[7:0] == 0;
              flag_negative <= data[7];
            end
            4: register_file_write <= 1;
            5: reset_to_state_0_at_end_of_cycle = 1;
            endcase

          end

          3:
          begin
            case (clocks_in_this_state)
            1: address_bus_execution <= calculated_address_Y;
            3:
            begin
              case (minor_mode)
                0: begin register_file_port1_select <= `REGFILE_A; register_file_write_data <= data_bus_in; end
                1: begin register_file_port1_select <= `REGFILE_X; register_file_write_data <= data_bus_in; end
                default: ;
              endcase

              flag_zero <= data[7:0] == 0;
              flag_negative <= data[7];
            end
            4: register_file_write <= 1;
            5: reset_to_state_0_at_end_of_cycle = 1;
            endcase
          end

          5:
          begin
            case (clocks_in_this_state)
            2: temp16_value[7:0] <= data_bus_in;
            3: address_bus_execution <= address_plus_one;
            5: begin temp16_value[15:8] <= data_bus_in; register_file_port1_select <= `REGFILE_Y; end
            6: address_bus_execution <= temp16_value + register_file_port1_output;
            8: begin register_file_write_data <= data_bus_in; register_file_port1_select <= `REGFILE_A; end
            9: register_file_write <= 1;
            10: reset_to_state_0_at_end_of_cycle <= 1;
            default: ;
            endcase
          end

          default: ;
          endcase

        end

        else // special: Minor Mode 2
        begin
          case (addressing_mode)
          1: // INC Abs16
          begin
            case (clocks_in_this_state)
            3: begin data_bus_out <= (data_bus_in + 1); write_operation <= 1; end
            5: reset_to_state_0_at_end_of_cycle = 1;
            default: ;
            endcase
          end

          2: // DEC Abs16
          begin
            case (clocks_in_this_state)
            3: begin data_bus_out <= (data_bus_in - 1); write_operation <= 1; end
            5: reset_to_state_0_at_end_of_cycle = 1;
            default: ;
            endcase
          end

          default: ;
          endcase
        end
      end

      // Major Mode 2
      2:
      begin

        case (addressing_mode)

        1:
        begin
          case (clocks_in_this_state)
          1:
          begin
            case (minor_mode)
            0: register_file_port1_select <= `REGFILE_A;
            1: register_file_port1_select <= `REGFILE_X;
            3: register_file_port1_select <= `REGFILE_Y;
            endcase
          end
          2: data_bus_out <= register_file_port1_output;
          3: write_operation <= 1;
          5: reset_to_state_0_at_end_of_cycle <= 1;
          endcase
        end


        2:
        begin
          case (clocks_in_this_state)
          1:
          begin
            address_bus_execution <= calculated_address_X;
            case (minor_mode)
            0: register_file_port1_select <= `REGFILE_A;
            1: register_file_port1_select <= `REGFILE_X;
            3: register_file_port1_select <= `REGFILE_Y;
            endcase
          end
          3: data_bus_out <= register_file_port1_output;
          4: write_operation <= 1;
          6: reset_to_state_0_at_end_of_cycle <= 1;
          endcase
        end

        endcase
      end

      3:
      begin
        if(minor_mode == 0) // ADC
        begin
          case (clocks_in_this_state)
          1:
          begin
            register_file_port1_select <= `REGFILE_A;
            alu_ctrl_input_A <= `ALU_INPUT_REG_A;
            alu_operation <= `ALU_OP_ADC;
            alu_use_control_lines <= 1;

            case (addressing_mode)
            0: alu_ctrl_input_B <= `ALU_INPUT_DATAREG;
            1: alu_ctrl_input_B <= `ALU_INPUT_DATABUS;
            2: alu_ctrl_input_B <= `ALU_INPUT_DATABUS;
            3: alu_ctrl_input_B <= `ALU_INPUT_DATABUS;
            endcase
          end
          2: alu_GO <= 1;
          3: register_file_write_data <= alu_R;
          4: register_file_write <= 1;
          5: begin update_flags_from_alu <= 1; reset_to_state_0_at_end_of_cycle <= 1; end
          endcase
        end

        else if(minor_mode == 1) // CMP
        begin
          case (addressing_mode)
          0: // CMP Imm8
          begin
            case (clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_A;
            2: temp_value = register_file_port1_output - data[7:0];
            3:
              begin
              flag_carry = temp_value <= register_file_port1_output;
              update_cflags_zn(temp_value);

              reset_to_state_0_at_end_of_cycle <= 1;
              end
            endcase
          end

          1: // CMP Abs16
          begin
            case(clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_A;
            2:
            begin
              temp_value <= register_file_port1_output - data_bus_in;
            end
            3: begin flag_carry <= temp_value <= register_file_port1_output; update_cflags_zn(temp_value); end
            4: reset_to_state_0_at_end_of_cycle <= 1;
            endcase
          end

          default: ;
          endcase
        end

        else if(minor_mode == 2) // SBC
        begin
          case (clocks_in_this_state)
          1:
          begin
            register_file_port1_select <= `REGFILE_A;
            alu_ctrl_input_A <= `ALU_INPUT_REG_A;
            alu_operation <= `ALU_OP_SBC;
            alu_use_control_lines <= 1;

            case (addressing_mode)
            0: alu_ctrl_input_B <= `ALU_INPUT_DATAREG;
            1: alu_ctrl_input_B <= `ALU_INPUT_DATABUS;
            2: alu_ctrl_input_B <= `ALU_INPUT_DATABUS;
            3: alu_ctrl_input_B <= `ALU_INPUT_DATABUS;
            endcase
          end
          2: alu_GO <= 1;
          3: register_file_write_data <= alu_R;
          4: register_file_write <= 1;
          5: begin update_flags_from_alu <= 1; reset_to_state_0_at_end_of_cycle <= 1; end
          endcase
        end

        else if(minor_mode == 3) // CPX and CPY
        begin

          case (addressing_mode)
          0: // CPX Imm8
          begin
            case (clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_X;
            2: temp_value = register_file_port1_output - data[7:0];
            3:
              begin
              flag_carry = temp_value <= register_file_port1_output;
              update_cflags_zn(temp_value);

              reset_to_state_0_at_end_of_cycle <= 1;
              end
            endcase
          end
          1: // CPX Abs16
          begin
            case(clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_X;
            2:
            begin
              temp_value <= register_file_port1_output - data_bus_in;
            end
            3: begin flag_carry <= temp_value <= register_file_port1_output; update_cflags_zn(temp_value); end
            4: reset_to_state_0_at_end_of_cycle <= 1;
            endcase
          end
          // 2 is unused
          4:  // CPY Imm8
          begin
            case (clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_Y;
            2: temp_value = register_file_port1_output - data[7:0];
            3:
              begin
              flag_carry = temp_value <= register_file_port1_output;
              update_cflags_zn(temp_value);

              reset_to_state_0_at_end_of_cycle <= 1;
              end
            endcase
          end
          3: // CPY Abs16
          begin
            case(clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_Y;
            2:
            begin
              temp_value <= register_file_port1_output - data_bus_in;
            end
            3: begin flag_carry <= temp_value <= register_file_port1_output; update_cflags_zn(temp_value); end
            4: reset_to_state_0_at_end_of_cycle <= 1;
            endcase
          end
          endcase
        end
      end

      4:
      begin
        case (minor_mode)
        0:
        begin
          case (clocks_in_this_state)
          1:
          begin
            case (addressing_mode)
            0: ;
            1: ;
            2: address_bus_execution <= calculated_address_X;
            3: address_bus_execution <= calculated_address_Y;
            default: ;
            endcase
          end
          3:
          begin
            case (addressing_mode)
            0: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            1: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            2: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            3: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            default: ;
            endcase

            alu_operation <= `ALU_OP_ASL;
            alu_use_control_lines <= 1;
          end
          4: alu_GO <= 1;
          5:
          begin
            case (addressing_mode)
            0: register_file_write_data <= alu_R;
            1: begin data_bus_out <= alu_R; write_operation <= 1; end
            2: begin data_bus_out <= alu_R; write_operation <= 1; end
            3: begin data_bus_out <= alu_R; write_operation <= 1; end
            endcase
            update_flags_from_alu <= 1;
          end
          6:
          begin
          case (addressing_mode)
            0: register_file_write <= 1;
            default: ;
          endcase
          end
          7: reset_to_state_0_at_end_of_cycle <= 1;
          endcase
        end


        1:
        begin
          case (clocks_in_this_state)
          1:
          begin
            case (addressing_mode)
            0: ;
            1: ;
            2: address_bus_execution <= calculated_address_X;
            3: address_bus_execution <= calculated_address_Y;
            default: ;
            endcase
          end
          3:
          begin
            case (addressing_mode)
            0: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            1: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            2: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            3: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            default: ;
            endcase

            alu_operation <= `ALU_OP_LSR;
            alu_use_control_lines <= 1;
          end
          4: alu_GO <= 1;
          5:
          begin
            case (addressing_mode)
            0: register_file_write_data <= alu_R;
            1: begin data_bus_out <= alu_R; write_operation <= 1; end
            2: begin data_bus_out <= alu_R; write_operation <= 1; end
            3: begin data_bus_out <= alu_R; write_operation <= 1; end
            endcase
            update_flags_from_alu <= 1;
          end
          6:
          begin
          case (addressing_mode)
            0: register_file_write <= 1;
            default: ;
          endcase
          end
          7: reset_to_state_0_at_end_of_cycle <= 1;
          endcase
        end

        2:
        begin
          case (clocks_in_this_state)
          1:
          begin
            case (addressing_mode)
            0: ;
            1: ;
            2: address_bus_execution <= calculated_address_X;
            3: address_bus_execution <= calculated_address_Y;
            default: ;
            endcase
          end
          3:
          begin
            case (addressing_mode)
            0: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATAREG; end
            1: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATABUS; end
            2: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATABUS; end
            3: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATABUS; end
            default: ;
            endcase

            alu_operation <= `ALU_OP_OR;
            alu_use_control_lines <= 1;
          end
          4: alu_GO <= 1;
          5:
          begin
            register_file_write_data <= alu_R;
            update_flags_from_alu <= 1;
          end
          6: register_file_write <= 1;
          7: reset_to_state_0_at_end_of_cycle <= 1;
          endcase
        end

        3:
        begin
          case (clocks_in_this_state)
          1:
          begin
            case (addressing_mode)
            0: ;
            1: ;
            2: address_bus_execution <= calculated_address_X;
            3: address_bus_execution <= calculated_address_Y;
            default: ;
            endcase
          end
          3:
          begin
            case (addressing_mode)
            0: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATAREG; end
            1: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATABUS; end
            2: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATABUS; end
            3: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATABUS; end
            default: ;
            endcase

            alu_operation <= `ALU_OP_AND;
            alu_use_control_lines <= 1;
          end
          4: alu_GO <= 1;
          5:
          begin
            register_file_write_data <= alu_R;
            update_flags_from_alu <= 1;
          end
          6: register_file_write <= 1;
          7: reset_to_state_0_at_end_of_cycle <= 1;
          endcase
        end
        endcase
      end

      5:
      begin
        case (minor_mode)
        0:
        begin
          case (clocks_in_this_state)
          1:
          begin
            case (addressing_mode)
            0: ;
            1: ;
            2: address_bus_execution <= calculated_address_X;
            3: address_bus_execution <= calculated_address_Y;
            default: ;
            endcase
          end
          3:
          begin
            case (addressing_mode)
            0: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATAREG; end
            1: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATABUS; end
            2: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATABUS; end
            3: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_DATABUS; end
            default: ;
            endcase

            alu_operation <= `ALU_OP_XOR;
            alu_use_control_lines <= 1;
          end
          4: alu_GO <= 1;
          5:
          begin
            register_file_write_data <= alu_R;
            update_flags_from_alu <= 1;
          end
          6: register_file_write <= 1;
          7: reset_to_state_0_at_end_of_cycle <= 1;
          endcase
        end


        1:
        begin
          case (clocks_in_this_state)
          1:
          begin
            case (addressing_mode)
            0: ;
            1: ;
            2: address_bus_execution <= calculated_address_X;
            3: address_bus_execution <= calculated_address_Y;
            default: ;
            endcase
          end
          3:
          begin
            case (addressing_mode)
            0: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            /*
            1: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            2: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            3: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            */
            default: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            endcase

            alu_operation <= `ALU_OP_ROL;
            alu_use_control_lines <= 1;
          end
          4: alu_GO <= 1;
          5:
          begin
            case (addressing_mode)
            0: register_file_write_data <= alu_R;
            /*
            1: begin data_bus_out <= alu_R; write_operation <= 1; end
            2: begin data_bus_out <= alu_R; write_operation <= 1; end
            3: begin data_bus_out <= alu_R; write_operation <= 1; end
            */
            default: begin data_bus_out <= alu_R; write_operation <= 1; end
            endcase
          end
          6:
          begin
          case (addressing_mode)
            0: register_file_write <= 1;
            default: ;
          endcase
          end
          7: begin update_flags_from_alu <= 1; reset_to_state_0_at_end_of_cycle <= 1; end
          default: ;
          endcase
        end

        2:
        begin
          case (clocks_in_this_state)
          1:
          begin
            case (addressing_mode)
            2: address_bus_execution <= calculated_address_X;
            3: address_bus_execution <= calculated_address_Y;
            default: ;
            endcase
          end
          3:
          begin
            case (addressing_mode)
            0: begin register_file_port1_select <= `REGFILE_A; alu_ctrl_input_A <= `ALU_INPUT_REG_A; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            /*
            1: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            2: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            3: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            */
            default: begin alu_ctrl_input_A <= `ALU_INPUT_DATABUS; alu_ctrl_input_B <= `ALU_INPUT_ONE; end
            endcase

            alu_operation <= `ALU_OP_ROR;
            alu_use_control_lines <= 1;
          end
          4: alu_GO <= 1;
          5:
          begin
            case (addressing_mode)
            0: register_file_write_data <= alu_R;
            default: begin data_bus_out <= alu_R; write_operation <= 1; end
            /*
            1: begin data_bus_out <= alu_R; write_operation <= 1; end
            2: begin data_bus_out <= alu_R; write_operation <= 1; end
            3: begin data_bus_out <= alu_R; write_operation <= 1; end
            */
            endcase
          end
          6:
          begin
          case (addressing_mode)
            0: register_file_write <= 1;
            default: ;
          endcase
          end
          7: begin update_flags_from_alu <= 1; reset_to_state_0_at_end_of_cycle <= 1; end
          default: ;
          endcase
        end
        endcase
      end

      // branches
      6:
      begin
        case (minor_mode)
        0:
        begin
          case (clocks_in_this_state)
          2:
            case (addressing_mode)
            0: temp_bit1 <= 1; // BRA
            1: if (flag_carry == 0) temp_bit1 <= 1; else temp_bit1 <= 0;  // BCC
            2: if (flag_carry) temp_bit1 <= 1; else temp_bit1 <= 0;       // BCS
            3: if (flag_zero) temp_bit1 <= 1; else temp_bit1 <= 0;        // BEQ
            4: if (flag_zero == 0) temp_bit1 <= 1; else temp_bit1 <= 0;   // BNE
            5: if (flag_overflow == 0) temp_bit1 <= 1; else temp_bit1 <= 0; // BVC
            6: if (flag_overflow) temp_bit1 <= 1; else temp_bit1 <= 0;    // BVS
            7: if (flag_negative) temp_bit1 <= 1; else temp_bit1 <= 0;    // BMI
            endcase
          3:
            if(temp_bit1)
            begin
              case (!data[7])
              1: pc <= pc + data[6:0];
              0: pc <= pc - data[6:0];
              default: ;
              endcase
            end
          4: reset_to_state_0_at_end_of_cycle <= 1;
          endcase
        end

        2:
        begin
          case (addressing_mode)
          0: // PHA - push A onto stack
          begin
            case (clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_A;
            2: begin address_bus_execution <= reg_SP; data_bus_out <= register_file_port1_output; end
            4: write_operation <= 1;
            5: begin reg_SP <= reg_SP - 1; reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
          end
          1: // PHX
          begin
            case (clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_X;
            2: begin address_bus_execution <= reg_SP; data_bus_out <= register_file_port1_output; end
            4: write_operation <= 1;
            5: begin reg_SP <= reg_SP - 1; reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
          end
          2: // PHY - push A onto stack
          begin
            case (clocks_in_this_state)
            1: register_file_port1_select <= `REGFILE_Y;
            2: begin address_bus_execution <= reg_SP; data_bus_out <= register_file_port1_output; end
            4: write_operation <= 1;
            5: begin reg_SP <= reg_SP - 1; reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
          end
          3: // PHP
          begin
            case (clocks_in_this_state)
            2: begin address_bus_execution <= reg_SP; data_bus_out <= condition_flags; end
            4: write_operation <= 1;
            5: begin reg_SP <= reg_SP - 1; reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
          end

          4: // PLA - pull stack into A
          begin
            case (clocks_in_this_state)
            1: begin reg_SP <= reg_SP + 1; register_file_port1_select <= `REGFILE_A; end
            2: address_bus_execution <= reg_SP;
            4: register_file_write_data <= data_bus_in;
            5: register_file_write <= 1;
            6: begin update_cflags_zn(register_file_port1_output); reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
          end
          5: // PLX
          begin
            case (clocks_in_this_state)
            1: begin reg_SP <= reg_SP + 1; register_file_port1_select <= `REGFILE_X; end
            2: address_bus_execution <= reg_SP;
            4: register_file_write_data <= data_bus_in;
            5: register_file_write <= 1;
            6: begin update_cflags_zn(register_file_port1_output); reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
          end
          6: // PLY
          begin
            case (clocks_in_this_state)
            1: begin reg_SP <= reg_SP + 1; register_file_port1_select <= `REGFILE_Y; end
            2: address_bus_execution <= reg_SP;
            4: register_file_write_data <= data_bus_in;
            5: register_file_write <= 1;
            6: begin update_cflags_zn(register_file_port1_output); reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
          end
          7: // PLP
          begin
            case (clocks_in_this_state)
            1: begin reg_SP <= reg_SP + 1; end
            2: address_bus_execution <= reg_SP;
            4: begin flag_negative <= data_bus_in[7]; flag_overflow <= data_bus_in[6]; flag_carry <= data_bus_in[5]; flag_zero <= data_bus_in[4]; flag_interrupts_off <= data_bus_in[3]; end
            5: begin reset_to_state_0_at_end_of_cycle <= 1; end
            endcase
          end
          endcase

        end

        default: ;
        endcase
      end

      // jumps
      7:
      begin

        case (minor_mode)
        0:
          case (addressing_mode)
          1: // JMP
          begin
            pc <= data[15:0];
            reset_to_state_0_at_end_of_cycle <= 1;
          end
          2: // JSR
          begin
            case (clocks_in_this_state)
            2:
            begin
              address_bus_execution <= reg_SP;
              data_bus_out <= pc[15:8];
            end
            3: write_operation <= 1;
            4:
            begin
              //write_operation <= 0;
              address_bus_execution <= sp_minus_one;
              data_bus_out <= pc[7:0];
            end
            5: write_operation <= 1;
            6: begin pc <= data[15:0]; reg_SP <= reg_SP - 2; reset_to_state_0_at_end_of_cycle = 1; end
            default: ;
            endcase
          end

          default: ;
          endcase

        default: ;
        endcase

      end
      endcase

      // Update ALU control lines.
      if(alu_use_control_lines)
      begin
        case (alu_ctrl_input_A)
        0: alu_A <= register_file_port1_output;
        1: alu_A <= register_file_port1_output;
        2: alu_A <= register_file_port1_output;
        3: alu_A <= data_bus_in;
        4: alu_A <= data[7:0];
        5: alu_A <= 1;
        6: ;
        7: ;
        endcase

        case (alu_ctrl_input_B)
        0: alu_B <= register_file_port2_output;
        1: alu_B <= register_file_port2_output;
        2: alu_B <= register_file_port2_output;
        3: alu_B <= data_bus_in;
        4: alu_B <= data[7:0];
        5: alu_B <= 1;
        6: ;
        7: ;
        endcase
      end

      if (update_flags_from_alu)
      begin
        flag_carry <= alu_flag_C;
        flag_zero <= alu_flag_Z;
        flag_negative <= alu_flag_N;
        flag_overflow <= alu_flag_V;
        update_flags_from_alu <= 0;
      end

    end
    // END Execute
    endcase

    if (reset_to_state_0_at_end_of_cycle) reset_to_state_0;

    // Advance to the next CPU state if required.
    if(advance_state)
    begin
      case (cpu_state)
        0: cpu_state <= 1;
        1: cpu_state <= 0;
        default: cpu_state <= 0;
      endcase
      clocks_in_this_state <= 0;
      advance_state <= 0;
      write_operation <= 0;
    end

  end
  end

endmodule
