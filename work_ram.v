module work_ram (
  input rclock,
  input wclock,

  input ren,
  input wen,

  input [7:0] raddr,
  input [7:0] waddr,

  input [7:0] data_bus_in,
  output reg [7:0] data_bus_out
);


reg [7:0] mem[255:0];

integer i;
initial begin
  for(i=0; i<256; i++) mem[i] <= 255 - i;
end

always @(posedge wclock) begin
  if(wen) mem[waddr] <= data_bus_in;
end

always @(posedge rclock) begin
  if(ren) data_bus_out <= mem[raddr];
end

endmodule
