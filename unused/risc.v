`include "includes/cpudefines.vh"

`define CHANGE_CPU_STATE(N) cpu_state <= N; clocks_in_this_state <= 0;

module risc (
  input clock,
  output [31:0] address_bus,

  input [31:0] data_bus_in,
  output [31:0] data_bus_out,
  output [1:0] data_op_width,

  output [1:0] mux_select,

  output rw                  // high if this is a write operation
  );

  // Effective 8MHz clock.
  reg clock_divider = 1;

  reg [3:0] clocks_in_this_state = 0;
  reg [3:0] cpu_state = 0;
  reg advance_state = 0;
  reg reset_state_0 = 0;

  // Registers
  reg register_write_enable;
  reg [4:0] register_address_rs1, register_address_rs2, register_address_rd;
  reg [31:0] register_data_in;

  wire [31:0] register_data_rs1_out, register_data_rs2_out, register_data_rd_out;

  risc_register_file risc_register_file
  (
    .clock(clock),
    .wen_rd(register_write_enable),

    .addr_rs1(register_address_rs1),
    .addr_rs2(register_address_rs2),
    .addr_rd(register_address_rd),

    .data_bus_rd_in(register_data_in),

    .data_bus_rs1_out(register_data_rs1_out),
    .data_bus_rs2_out(register_data_rs2_out),
    .data_bus_rd_out(register_data_rd_out)
  );

  reg [31:0] pc = 0;
  reg [31:0] ireg = 0;

  // State latches.
  reg write_operation = 0;
  assign rw = write_operation;

  //reg [31:0] address_latch;
  //assign address_bus = address_latch;

  reg [31:0] data_out_latch;
  assign data_bus_out = data_out_latch;

  reg [1:0] mux_select_latch;
  assign mux_select = mux_select_latch;

  reg [1:0] data_op_width_latch;
  assign data_op_width = data_op_width_latch;

  reg [31:0] address_bus_execution, address_bus_fetch, address_bus_memoryaccess, address_bus_writeback;
  reg [1:0] address_bus_mux_selector;

  four_to_one_mux #(.BUS_WIDTH(32)) address_bus_source_mux
  (
    .a(address_bus_fetch),
    .b(address_bus_execution),
    .c(address_bus_memoryaccess),
    .d(address_bus_writeback),
    .out(address_bus),
    .select(address_bus_mux_selector)
  );

  // Instruction fields.
  wire [4:0] op_reg_source1, op_reg_source2, op_reg_dest;
  wire [2:0] op_column;
  wire [1:0] op_row;
  wire [2:0] op_func3;
  wire [6:0] op_func7;
  wire [19:0] op_immediate;

  reg [31:0] operand_intermediate;

  risc_decoder decoder(
    .clock(clock_divider),
    .instruction(ireg),
    .reg_select_src1(op_reg_source1),
    .reg_select_src2(op_reg_source2),
    .reg_select_dest(op_reg_dest),
    .opcode_column(op_column),
    .opcode_row(op_row),
    .function7(op_func7),
    .function3(op_func3),
    .immediate(op_immediate)
  );

  always @(posedge clock)
  begin
    clock_divider <= ~clock_divider;
    clocks_in_this_state <= clocks_in_this_state + 1;

    // Fetch Stage
    if (cpu_state == `CPU_STATE_FETCH)
    begin
      address_bus_mux_selector = `CPUMUX_FETCH;

      if(clocks_in_this_state == 1)
      begin
        // Read four bytes from [pc].
        write_operation <= 0;
        address_bus_fetch <= pc;
        mux_select_latch <= 1;
        data_op_width_latch <= `WIDTH32;
      end
      else if(clocks_in_this_state == 2)
      begin
        // The data bus is now populated with the value from RAM.

        // Store the instruction.
        ireg <= data_bus_in;
        // Update PC.
        pc <= pc + 4;

        advance_state <= 1;
      end
    end

    else if (cpu_state == `CPU_STATE_DECODE)
    begin
      // Wait one internal cycle for the decoder.
      // The decoder should populate from the decoded instruction automatically.
      advance_state <= 1;
    end

    // Execute Stage
    else if (cpu_state == `CPU_STATE_EXECUTE)
    begin
      address_bus_mux_selector = `CPUMUX_EXECUTE;

      if(op_row == 0)
      begin

        if(op_column == 0)
        begin
          // LOAD instructions don't use the ALU.
          advance_state = 1;
        end

        else if(op_column == 1)
        begin
          // LOAD-FP
        end

        else if(op_column == 2)
        begin
          // custom-0
        end

        else if(op_column == 3)
        begin
          // MISC-MEM
        end

        else if(op_column == 4)
        begin
          // OP-IMM
          if(clocks_in_this_state == 1)
          begin
            register_address_rd = op_reg_dest;
            register_address_rs1 = op_reg_source1;
          end
          else if(clocks_in_this_state == 2)
          begin
            // The register file will now have our result.
            if(op_func3 == 0)
            begin
              // ADDI: rd = rs1 + imm
              if(op_immediate[11])
                operand_intermediate = op_immediate | 32'hFFFFF000;
              else
                operand_intermediate = op_immediate;
              register_data_in = register_data_rs1_out + operand_intermediate;
              register_write_enable = 1;

              reset_state_0 = 1;
            end
          end
        end

        else if(op_column == 5)
        begin
          // AUIPC
          pc <= pc + op_immediate;
        end

        else if(op_column == 6)
        begin
          // OP-IMM-32
        end

      end

      else if(op_row == 1)
      begin

        if(op_column == 0)
        begin
          // STOREs don't use the ALU.
        end

        else if(op_column == 1)
        begin
          // STORE-FP
        end

        else if(op_column == 2)
        begin
          // custom-1
        end

        else if(op_column == 3)
        begin
          // AMO
        end

        else if(op_column == 4)
        begin
          // OP
        end

        else if(op_column == 5)
        begin
          // LUI
        end

        else if(op_column == 6)
        begin
          // OP-32
        end

      end

      else if(op_row == 2)
      begin

      end

      else if(op_row == 3)
      begin

      end
    end

    // Memory Access
    else if (cpu_state == `CPU_STATE_MEMORYACCESS)
    begin
      address_bus_mux_selector = `CPUMUX_MEMORYACCESS;

      if(op_row == 0)
      begin

        if(op_column == 0)
        begin
          // LOADs - LB LH LW LBU LHU.
          if(clocks_in_this_state == 1)
            register_address_rs1 <= op_reg_source1;
          else if(clocks_in_this_state == 2)
          begin
            address_bus_memoryaccess <= op_immediate + register_data_rs1_out; // imm + rs1
            advance_state <= 1;
          end
        end

        else if(op_column == 1)
        begin

        end

        else if(op_column == 2)
        begin

        end

        else if(op_column == 3)
        begin

        end

        else if(op_column == 4)
        begin
          advance_state = 1;
        end

        else if(op_column == 5)
        begin

        end

      end

      else if(op_row == 1)
      begin

        if(op_column == 0)
        begin
          // STOREs. All have the same format.
          register_address_rs1 <= op_reg_source1;
          register_address_rs2 <= op_reg_source2;
          register_address_rd <= op_reg_dest;
          advance_state <= 1;
        end

        else if(op_column == 1)
        begin

        end

        else if(op_column == 2)
        begin

        end

        else if(op_column == 3)
        begin

        end

        else if(op_column == 4)
        begin

        end

        else if(op_column == 5)
        begin
          register_address_rd <= op_reg_dest;
          advance_state <= 1;
        end

      end

      else if(op_row == 2)
      begin

      end

      else if(op_row == 3)
      begin

      end
    end

    else if(cpu_state == `CPU_STATE_WRITEBACK)
    begin
      // Writeback cycle 1
      address_bus_mux_selector = `CPUMUX_WRITEBACK;

      if(op_row == 0)
      begin

        if(op_column == 0)
        begin
          // LOADs
          if(op_func3 == 0)
          begin
            // LB
            // rd <= data in, sign-extended to 32 bits
            register_address_rd <= op_reg_dest;
            register_data_in <= data_bus_in;
            if(register_data_in[7]) register_data_in[31:8] <= ~register_data_in[31:16];
            register_write_enable = 1;
          end
          else if(op_func3 == 1)
          begin
            // LH
            // rd <= data in, sign-extended to 32 bits
            register_address_rd = op_reg_dest;
            register_data_in = data_bus_in;
            if(register_data_in[15]) register_data_in[31:16] = ~register_data_in[31:16];
            register_write_enable = 1;
          end
          else if(op_func3 == 2)
          begin
            // LW
            // rd <= data in
            register_address_rd = op_reg_dest;
            register_data_in = data_bus_in;
            register_write_enable = 1;
          end
          else if(op_func3 == 4 || op_func3 == 5)
          begin
            // LBU and LHU
            register_address_rd = op_reg_dest;
            register_data_in = data_bus_in & 32'h000000FF;
            register_write_enable = 1;
          end
        end

        else if(op_column == 1)
        begin

        end

        else if(op_column == 2)
        begin

        end

        else if(op_column == 3)
        begin

        end

        else if(op_column == 4)
        begin
          advance_state = 1;
        end

        else if(op_column == 5)
        begin

        end

      end

      else if(op_row == 1)
      begin

        if(op_column == 0)
        begin
          // STOREs
          address_bus_writeback = register_data_rs1_out + op_immediate;
          data_out_latch = register_data_rs2_out;
          if(op_func3 == 0)       // SB
          begin
            data_op_width_latch = `WIDTH8;
          end
          else if(op_func3 == 1)  // SH
          begin
            data_op_width_latch = `WIDTH16;
          end
          else if(op_func3 == 2)  // SW
          begin
            data_op_width_latch = `WIDTH32;
          end

          write_operation <= 1;
          advance_state <= 1;
        end

        else if(op_column == 1)
        begin

        end

        else if(op_column == 2)
        begin

        end

        else if(op_column == 3)
        begin

        end

        else if(op_column == 4)
        begin

        end

        else if(op_column == 5)
        begin
          // LUI
          register_data_in <= (op_immediate[15:0]) << 16;
          register_write_enable = 1;
          advance_state <= 1;
        end

      end

      else if(op_row == 2)
      begin

      end

      else if(op_row == 3)
      begin

      end
    end

    // Advance to the next CPU state if required.
    if(advance_state)
    begin
      case (cpu_state)
        0: cpu_state <= 1;
        1: cpu_state <= 2;
        2: cpu_state <= 3;
        3: cpu_state <= 4;
        4: cpu_state <= 0;
        default: cpu_state <= 0;
      endcase
      clocks_in_this_state <= 1;
      advance_state <= 0;
    end

    // Reset to state 0.
    if(reset_state_0)
    begin
      cpu_state <= 0;
      clocks_in_this_state <= 1;
    end

  end

endmodule
