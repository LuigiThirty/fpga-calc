`include "includes/cpudefines.vh"

module canis
(
  input clock,
  output [15:0] address_bus,

  input [7:0] data_bus_in,
  output [7:0] data_bus_out,
  output [1:0] data_op_width,

  input reset,
  input nmi,

  output rw                  // high if this is a write operation
);

// Condition flags
reg flag_negative = 0;
reg flag_overflow = 0;
reg flag_carry = 0;
reg flag_zero = 0;
reg flag_interrupts_off = 0;
wire [7:0] condition_flags;

assign condition_flags[7] = flag_negative;
assign condition_flags[6] = flag_overflow;
assign condition_flags[5] = flag_carry;
assign condition_flags[4] = flag_zero;
assign condition_flags[3] = flag_interrupts_off;
assign condition_flags[2] = 0;
assign condition_flags[1] = 0;
assign condition_flags[0] = 0;

/* Tasks */
task advance_pc;
    pc <= pc + 1;
endtask

task update_cflags_zn(input reg [7:0] Q);
  begin
    flag_zero = (Q == 0);
    flag_negative = Q[7];
  end
endtask

reg reset_to_state_0_at_end_of_cycle;
task reset_to_state_0;
  begin
    cpu_state <= 0;
    clocks_in_this_state <= 0;
    ireg <= 0;
    advance_state <= 0;
    write_operation <= 0;
    alu_GO <= 0;
    alu_use_control_lines <= 0;
    reset_to_state_0_at_end_of_cycle <= 0;
  end
endtask

`include "canis_execute.vh"

reg [7:0] obt_opcode;
wire [1:0] obt_operand_byte_count;
canis_opcode_byte_table opcode_byte_table
(
  .opcode(obt_opcode),
  .operand_byte_count(obt_operand_byte_count)
);

reg [3:0] alu_operation;
reg [7:0] alu_A, alu_B;
wire [7:0] alu_R;
wire alu_flag_N, alu_flag_V, alu_flag_C, alu_flag_Z;
reg alu_GO;
reg [3:0] alu_ctrl_input_A, alu_ctrl_input_B;
reg alu_use_control_lines;
reg update_flags_from_alu;

canis_alu alu
(
  .clock(clock),
  .operation(alu_operation),
  .go(alu_GO),
  .A(alu_A),
  .B(alu_B),
  .R(alu_R),
  .carry(flag_carry),

  .f_carry(alu_flag_C),
  .f_overflow(alu_flag_V),
  .f_negative(alu_flag_N),
  .f_zero(alu_flag_Z)
);

reg [3:0] clocks_in_this_state = 0;
reg cpu_state = 0;
reg advance_state = 0;

// Registers. SP = stack pointer, SV = System Vectors - page containing SYS vectors
// SA = Sys Args - pointer to location containing syscall arguments
reg [15:0] pc;
reg [15:0] reg_SP;
reg [7:0] reg_X, reg_Y, reg_A, reg_SV;

reg [31:0] ireg;

reg temp_bit1;
reg [7:0] temp_value;
reg [15:0] temp16_value;

// State latches
reg write_operation = 0;
assign rw = write_operation;

reg [7:0] data_out_latch;
assign data_bus_out = data_out_latch;

reg [1:0] data_op_width_latch;
assign data_op_width = data_op_width_latch;

reg [15:0] address_bus_execution, address_bus_fetch, address_bus_memoryaccess, address_bus_writeback = 0;
reg [1:0] address_bus_mux_selector;

four_to_one_mux #(.BUS_WIDTH(16)) address_bus_source_mux
(
  .a(address_bus_fetch),
  .b(address_bus_execution),
  .c(address_bus_memoryaccess),
  .d(address_bus_writeback),
  .out(address_bus),
  .select(address_bus_mux_selector)
);

// opcode map
// the high 3 bits define how many bytes the instruction is.
// instruction format is usually:
//   MMM FF AAA
//  where MMM = major mode, FF = minor function, and AAA = addressing mode

reg [2:0] operand_bytes_to_fetch = 0;

reg [2:0] minor_mode;
reg [3:0] addressing_mode;
reg [2:0] major_mode;
reg [15:0] data;

reg initialized;

initial begin
  initialized = 0;
end

//
wire [15:0] calculated_address_X;
wire [15:0] calculated_address_Y;
wire [15:0] address_plus_one;
wire [15:0] sp_plus_one, sp_minus_one, sp_plus_two, sp_minus_two;
canis_address_calculator address_calculator(
  .update(clock),

  .address(address_bus),

  .rSP(reg_SP),

  .rX(reg_X),
  .rY(reg_Y),

  .calculated_address_x(calculated_address_X),
  .calculated_address_y(calculated_address_Y),
  .address_plus_one(address_plus_one),

  .sp_plus_one(sp_plus_one),
  .sp_plus_two(sp_plus_two),
  .sp_minus_one(sp_minus_one)
);

always @(posedge clock)
begin
  if(!initialized)
  begin
    reg_SP <= 16'h0100;
    reg_A <= 0;
    reg_X <= 0;
    reg_Y <= 0;
    pc <= 16'hF000;

    ireg <= 0;

    temp_value <= 0;
    temp16_value <= 0;
    alu_GO <= 0;

    initialized <= 1;
  end

end

always @(posedge clock)
begin


endmodule
