`ifndef CPUDEFINES_VH
`define CPUDEFINES_VH

`define WIDTH8  0
`define WIDTH16 1
`define WIDTH32 2

// Mux select bits
`define MUX_OUT_SELECT_CPU 0
`define MUX_OUT_SELECT_RAM 1
// 2 is unassigned
// 3 is unassigned

// CPU state defines.
`define CPU_STATE_FETCH 0
`define CPU_STATE_EXECUTE 1

`define CPUMUX_FETCH 0
`define CPUMUX_EXECUTE 1
`define CPUMUX_MEMORYACCESS 2
`define CPUMUX_WRITEBACK 3

// Address range defines.
`define ADDRESS_RANGE(X,Y) (address_bus >= X && address_bus <= Y)
`define ADDRESS_MATCH(X) (address_bus == X)

// ALU operations
`define ALU_OP_ADD 1
`define ALU_OP_SUB 2
`define ALU_OP_ASL 3
`define ALU_OP_LSR 4
`define ALU_OP_OR  5
`define ALU_OP_AND 6
`define ALU_OP_XOR 7
`define ALU_OP_NOT 8
`define ALU_OP_ROL 9
`define ALU_OP_ROR 10
`define ALU_OP_ADC 11
`define ALU_OP_SBC 12

`define ALU_INPUT_REG_A 0
`define ALU_INPUT_REG_X 1
`define ALU_INPUT_REG_Y 2
`define ALU_INPUT_DATABUS 3 // data_bus_in
`define ALU_INPUT_DATAREG 4 // data[7:0]
`define ALU_INPUT_ONE 5     // literal 1

`define REGFILE_A 0
`define REGFILE_X 1
`define REGFILE_Y 2

`include "common/four_to_one_mux.v"

`endif
