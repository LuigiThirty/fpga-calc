module canis_alu
(
  input clock,
  input [3:0] operation,
  input go,

  // Inputs
  input [7:0] A,
  input [7:0] B,
  input carry,

  // Outputs
  output reg [7:0] R,

  // flags
  output reg f_carry,
  output reg f_zero,
  output reg f_overflow,
  output reg f_negative
);

reg temp;
reg go_has_gone_low_since_last_clock = 0;

task alu_add;
  begin
    R = A + B;
    f_carry <= (R[7] < A[7]);
    f_zero <= (R == 0);
    f_negative <= R[7];
  end
endtask

task alu_subtract;
  begin
    R = A - B;
    f_carry <= (R[7] < A[7]);
    f_zero <= (R == 0);
    f_negative <= R[7];
  end
endtask

task alu_arithmetic_shift_left;
  begin
    f_carry = A[7];
    R <= A << 1;
    f_zero <= (R == 0);
    f_negative <= R[7];
  end
endtask

task alu_logicaf_shift_right;
  begin
    f_carry = A[0];
    R <= A >> 1;
    f_zero <= (R == 0);
    f_negative <= R[7];
  end
endtask

task alu_or;
  begin
    R = A | B;
    f_zero <= (R == 0);
    f_negative <= R[7];
  end
endtask

task alu_and;
  begin
    R = A & B;
    f_zero <= (R == 0);
    f_negative <= R[7];
  end
endtask

task alu_xor;
  begin
    R = A ^ B;
    f_zero <= (R == 0);
    f_negative <= R[7];
  end
endtask

task alu_not;
  begin
    R = ~A;
    f_zero <= (R == 0);
    f_negative <= R[7];
  end
endtask

task alu_rol;
  begin
    f_carry = A[7];
    R = A << 1;
    R[0] = carry;
  end
endtask

task alu_ror;
  begin
    f_carry = A[0];
    R = A >> 1;
    R[7] = carry;
  end
endtask

task alu_add_carry;
  begin
    R = A + B + carry;
    f_carry <= (R[7] < A[7]);
    f_zero <= (R == 0);
    f_negative <= R[7];
  end
endtask

task alu_subtract_carry;
  begin
    R = A - B - (1-carry);
    f_carry <= (R[7] < A[7]);
    f_zero <= (R == 0);
    f_negative <= R[7];
  end
endtask

/* ALU operations table */
always @(negedge clock) begin
  if(go && go_has_gone_low_since_last_clock)
  begin
    case (operation)
      1: alu_add;
      2: alu_subtract;
      3: alu_arithmetic_shift_left;
      4: alu_logicaf_shift_right;
      5: alu_or;
      6: alu_and;
      7: alu_xor;
      8: alu_not;
      9: alu_rol;
      10: alu_ror;
      11: alu_add_carry;
      12: alu_subtract_carry;
      default: ; // do nothing
    endcase
    go_has_gone_low_since_last_clock <= 0;
  end
  else if(!go) go_has_gone_low_since_last_clock <= 1;
end

endmodule
