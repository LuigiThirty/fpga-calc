module fixed_16_adder(
  input  [15:0] A,  // A
  input  [15:0] B,  // B
  output [15:0] C,  // RESULT
  output carry      // carry bit
  );

// 16-bit fixed point adds are just straight adds.

assign C = A + B;
assign carry = A[15] != B[15];

endmodule
