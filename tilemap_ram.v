module tilemap_ram (
  input clock,
  input wen,
  input [11:0] addr,
  input [7:0] wdata,
  output reg [7:0] rdata,

  input [11:0] video_port_addr,
  output reg [7:0] video_port_rdata
);

  // 48x25 tilemap. One byte = one tile index.
  reg [7:0] mem[0:1199];

  integer i;
  initial begin
    for(i = 0; i < 1200; i++)
    begin
      mem[i] <= 0;
    end
  end

  /*
  initial begin
    mem[50] <= "0";
    mem[51] <= "1";
    mem[52] <= "2";
    mem[53] <= "3";
    mem[54] <= "4";
    mem[55] <= "5";
    mem[56] <= "6";
    mem[57] <= "7";
    mem[58] <= "8";
    mem[59] <= "9";

    mem[99] <=  "@";
    mem[100] <= "A";
    mem[101] <= "B";
    mem[102] <= "C";
    mem[103] <= "D";
    mem[104] <= "E";
    mem[105] <= "F";
    mem[106] <= "G";
    mem[107] <= "H";
    mem[108] <= "I";
    mem[109] <= "J";
    mem[110] <= "K";
    mem[111] <= "L";
    mem[112] <= "M";
    mem[113] <= "N";
    mem[114] <= "O";
    mem[115] <= "P";
    mem[116] <= "Q";
    mem[117] <= "R";
    mem[118] <= "S";
    mem[119] <= "T";
    mem[120] <= "U";
    mem[121] <= "V";
    mem[122] <= "W";
    mem[123] <= "X";
    mem[124] <= "Y";
    mem[125] <= "Z";

    mem[200] <= "H";
    mem[201] <= "E";
    mem[202] <= "L";
    mem[203] <= "L";
    mem[204] <= "O";
    mem[205] <= " ";
    mem[206] <= "W";
    mem[207] <= "O";
    mem[208] <= "R";
    mem[209] <= "L";
    mem[210] <= "D";
  end
  */

  always @(posedge clock) begin
    if(wen) mem[addr] <= wdata;
    //rdata = mem[addr];
    video_port_rdata = mem[video_port_addr];
  end

endmodule
