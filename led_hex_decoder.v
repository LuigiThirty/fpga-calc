// Converts a hex digit to the hex code that will drive a 7-segment LED.

module led_hex_decoder(hex_in, segments_out);

input [3:0] hex_in;
output reg [7:0] segments_out;

always @(*) begin
  case (hex_in)
    0: segments_out <= 8'h3F;
    1: segments_out <= 8'h06;
    2: segments_out <= 8'h5B;
    3: segments_out <= 8'h4F;
    4: segments_out <= 8'h66;
    5: segments_out <= 8'h6D;
    6: segments_out <= 8'h7D;
    7: segments_out <= 8'h07;
    8: segments_out <= 8'h7F;
    9: segments_out <= 8'h67;
    10: segments_out <= 8'h77;
    11: segments_out <= 8'h7C;
    12: segments_out <= 8'h39;
    13: segments_out <= 8'h5E;
    14: segments_out <= 8'h79;
    15: segments_out <= 8'h71;
    default: segments_out <= 8'h01;
  endcase
end

endmodule
