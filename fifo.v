module fifo_8
(
  input r_clock,
  input w_clock,

  input [7:0] in_data,
  input push_byte,

  output reg [7:0] out_data,
  input pull_byte,

  output full,
  output empty,

  input reset
);


parameter QueueDepth = 16;

reg [7:0] data_buffer [15:0];
reg [3:0] read_pointer, write_pointer; // current position in the buffer
reg [3:0] content_count = 0;

initial begin
  read_pointer <= 0;
  write_pointer <= 0;
  content_count <= 0;

  data_buffer[0] <= 255;
  data_buffer[1] <= 254;
end

assign full = content_count == QueueDepth;
assign empty = content_count == 0;

reg debounce_push_byte = 0, debounce_pull_byte = 0;

always @(posedge w_clock)
begin
  if(push_byte && !debounce_push_byte) begin
    data_buffer[write_pointer] <= in_data;
    //write_pointer = write_pointer + 1;
    debounce_push_byte <= 1;
  end

  else if(!push_byte)
    debounce_push_byte <= 0;
end

always @(posedge r_clock)
begin
  if(pull_byte && !debounce_pull_byte) begin
    //read_pointer = read_pointer + 1;
    debounce_pull_byte <= 1;
  end

  else if(!pull_byte)
    debounce_pull_byte <= 0;
end

always @(posedge r_clock)
begin
    out_data <= data_buffer[read_pointer];
end


always @(posedge w_clock)
begin
  if(push_byte && !pull_byte && !full) content_count <= content_count + 1;
  if(!push_byte && pull_byte && !empty) content_count <= content_count - 1;

  //if(push_byte) content_count = content_count + 1;
end

/*
always @(posedge clock)
begin
  if(push_byte && !debounce_push_byte)
  begin
    debounce_push_byte <= 1;

    if(!full)
    begin
      data_buffer[write_pointer] <= in_data;
      write_pointer = write_pointer + 1;
      content_count = content_count + 1;
    end

  end
  else if(!push_byte) debounce_push_byte = 0;

  if(pull_byte && !debounce_pull_byte)
  begin
    debounce_pull_byte <= 1;

    if(!empty)
    begin
      out_data = data_buffer[read_pointer];
      read_pointer = read_pointer + 1;
      content_count = content_count - 1;
    end

  end
  else if(!pull_byte) debounce_pull_byte = 0;
end
*/

endmodule
