module canis_register_file
(
  // 0: A, 1: X, 2: Y
  input [1:0] register_port1_select,
  input [1:0] register_port2_select,

  // 8-bit registers
  output [7:0] read_value_port1,
  output [7:0] read_value_port2,

  input write,
  input [7:0] write_data,

  input reset,

  output [7:0] reg_X_private,
  output [7:0] reg_Y_private
);
assign reg_X_private = register_X;
assign reg_Y_private = register_Y;

reg [7:0] register_A;
reg [7:0] register_X;
reg [7:0] register_Y;

initial begin
  register_A = 0;
  register_X = 0;
  register_Y = 0;
end

four_to_one_mux #(.BUS_WIDTH(8)) port1_output_mux(
  .a(register_A),
  .b(register_X),
  .c(register_Y),
  .d(8'h0),
  .out(read_value_port1),
  .select(register_port1_select)
  );

four_to_one_mux #(.BUS_WIDTH(8)) port2_output_mux(
  .a(register_A),
  .b(register_X),
  .c(register_Y),
  .d(8'h0),
  .out(read_value_port2),
  .select(register_port2_select)
  );

always @(posedge write)
begin
  // Writes are always to port 1. Port 2 is for the ALU.
  case (register_port1_select)
  0: register_A = write_data;
  1: register_X = write_data;
  2: register_Y = write_data;
  endcase
end


endmodule
