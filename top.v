// look in pins.pcf for all the pin names on the TinyFPGA BX board
`include "includes/cpudefines.vh"

`include "common/eight_to_one_mux.v"
`include "common/right_shiftreg.v"
`include "common/sr_latch.v"
`include "common/up_counter.v"

module top (
    input CLK,    // 16MHz clock
    //output LED,   // User/boot LED next to power LED
    output USBPU,  // USB pull-up resistor

    // UART pins
    input PIN_13,   // RX
    output PIN_12,  // TX

    // Composite video
    output PIN_9,   // SIGNAL
    output PIN_10,   // SYNC

    // display pins
    output PIN_14,
    output PIN_15,
    output PIN_16,
    output PIN_17,
    output PIN_18,
    output PIN_19,
    output PIN_20,

    output PIN_21,
    output PIN_22,
    output PIN_23,
    output PIN_24
);

  reg clock_div_2;
  always @(posedge CLK) clock_div_2 = clock_div_2 + 1;

  reg reset_line = 0;

  wire [15:0] address_bus;
  wire [7:0] data_bus;

  wire cpu_write_line;  // high if the CPU is writing data

  wire [7:0] mux_input_ram, mux_input_rom, mux_output;
  reg [7:0] mux_input_uart;
  wire [7:0] data_from_cpu;

  wire [2:0] mux_selector;
  eight_to_one_mux #(.BUS_WIDTH(8)) data_bus_mux(
    .a(8'h0), //.a(mux_input_cpu),
    .b(mux_input_ram),
    .c(8'h0), //.c(mux_input_tile_ram),
    .d(mux_input_rom),
    .e(mux_input_uart),
    .f(8'h0),
    .g(8'h0),
    .h(8'h0),
    .out(mux_output),
    .select(mux_selector)
  );

  //
  canis cpu (
    .clock(CLK),
    .address_bus(address_bus),    // 16-bit
    .data_bus_in(mux_output),     // 8-bit
    .data_bus_out(data_from_cpu), // 8-bit
    .write_operation(cpu_write_line),
    .reset(reset_line)
  );

  wire display_chip_select;
  wire tilemap_chip_select;
  wire ram_chip_select;

  // drive USB pull-up resistor to '0' to disable USB
  assign USBPU = 0;

  assign mux_selector =
    uart_chip_select ? 4 :
    rom_chip_select ? 3 :
    tilemap_chip_select ? 2 :
    ram_chip_select ? 1 :
    0;

  four_character_display display(
    .clock(CLK),
    .address_bus(address_bus),
    .data_bus_in(data_from_cpu),
    .chip_select(display_chip_select),

    .a(PIN_14),
    .b(PIN_15),
    .c(PIN_16),
    .d(PIN_17),
    .e(PIN_18),
    .f(PIN_19),
    .g(PIN_20),

    .digit0(PIN_21),
    .digit1(PIN_22),
    .digit2(PIN_23),
    .digit3(PIN_24)
    );
  //-----------------------------

  /* UART */
  wire uart_chip_select;
  reg [7:0] uart_tx_byte = "A";
  reg uart_tx_ready = 0;
  wire uart_rst = 0;
  wire uart_tx_done;
  uart_out uart_transmit(
    .data_in(uart_tx_byte),
    .ready(uart_tx_ready),
    .rst(uart_rst),
    .tx(PIN_12),
    .tx_done(uart_tx_done),
    .clk(CLK)
    );

  wire [7:0] uart_rx_byte;
  wire uart_rx_new_byte;
  wire uart_rx_new_byte_ready;
  reg uart_rx_read_new_byte = 0;
  uart_in uart_receive(
    .srl_in(PIN_13),
    .data_out(uart_rx_byte),
    .clk(CLK),
    .rst(uart_rst),
    .new_byte_ready(uart_rx_new_byte_ready),
    .read_new_byte(uart_rx_read_new_byte)
    );

  reg uart_rx_did_read_last_byte;
  reg uart_clock_counter = 0;
  wire [7:0] fifo_byte_out;
  reg uart_fifo_pull_byte = 0;
  wire fifo_full, fifo_empty;

  always @(posedge CLK)
  begin
    // UART address $0: read the last byte received, pulse the clear line
    if(address_bus[1:0] == 0 && uart_chip_select)
    begin
      if(uart_clock_counter == 0)
      begin
        uart_fifo_pull_byte = 1;
        mux_input_uart = fifo_byte_out;
        uart_clock_counter = 1;
      end
    end

    else if(address_bus[1:0] == 1 && uart_chip_select)
    begin
      mux_input_uart = (fifo_empty ? 8'h80 : 0) | (fifo_full ? 8'h40 : 0);
    end

    if(!uart_chip_select) begin uart_clock_counter = 0; uart_fifo_pull_byte = 0; end
  end


  fifo_8 uart_fifo(
    .r_clock(CLK),
    .w_clock(~CLK),

    .in_data(uart_rx_byte),
    .push_byte(uart_rx_new_byte_ready),

    .out_data(fifo_byte_out),
    .pull_byte(uart_fifo_pull_byte),

    .full(fifo_full),
    .empty(fifo_empty)
    );

  wire [9:0] VID_XPOS; // Current X position of the composite out.
  wire [8:0] VID_YPOS; // Current Y position of the composite out.
  wire draw_pixel;     // 1bpp draw signal. 1 = draw a pixel, 0 = don't draw a pixel.

  wire tilemap_draw_pixel;
  reg border_draw_pixel;

  // Draw if either the border or the tilemap says we need to.
  assign draw_pixel = tilemap_draw_pixel | border_draw_pixel;

  // Video module
  ntsc_composite_video ntsc_video_out(
    .clock(CLK),              // 16MHz dot clock.
    .ntsc_signal(PIN_9),      // NTSC video signal out.
    .ntsc_sync(PIN_10),       // NTSC sync signal out.
    .xpos(VID_XPOS),          // Set VID_XPOS each clock.
    .ypos(VID_YPOS),          // Set VID_YPOS each clock.
    .draw_pixel(draw_pixel)   // The draw input.
    );

  // draw a border
  always @(posedge CLK) begin
    if(VID_XPOS > 57 && VID_XPOS < 379 && VID_YPOS == 4) border_draw_pixel <= 1;
    else if(VID_XPOS > 57 && VID_XPOS < 379 && VID_YPOS == 206) border_draw_pixel <= 1;
    else if(VID_XPOS == 57 && VID_YPOS >= 4 && VID_YPOS <= 206) border_draw_pixel <= 1;
    else if(VID_XPOS == 379 && VID_YPOS >= 4 && VID_YPOS <= 206) border_draw_pixel <= 1;
    else border_draw_pixel <= 0;
  end

  // Tilemap stuff
  wire [11:0] tilemap_address;
  wire tilemap_write_en;
  wire [7:0] tile_number;
  wire tile_ram_chip_select;
  wire [7:0] tilemap_data_out;
  wire [7:0] tilemap_read_line;

  wire [11:0] video_fetch_address;
  wire [7:0] video_fetch_out;

  tilemap_ram tilemap_data(
    .clock(CLK),

    // from tilemap engine
    .wen(tilemap_write_en),
    .addr(tilemap_address),
    .wdata(tilemap_data_out),
    .rdata(tilemap_read_line),

    .video_port_addr(video_fetch_address),
    .video_port_rdata(tile_number)
    );

  tilemap_display tilemap(
    .clock(CLK),                              // 16MHz master clock.
    .xpos(VID_XPOS),                          // Current X position that we need.
    .ypos(VID_YPOS),                          // Current Y position that we need.
    .pixel(tilemap_draw_pixel),               //

    .tile_number_in(tile_number),             // The tile number from tile RAM that is in the currently drawing XY position.
    .tilemap_address_out(tilemap_address),
    .tilemap_data_out(tilemap_data_out),

    .video_address_out(video_fetch_address),

    .address_bus(address_bus),                // Connected to the address bus.
    .data_bus_in(data_from_cpu),
    .cs(tilemap_chip_select),
    .rw(cpu_write_line),

    .tilemap_write_en(tilemap_write_en)       // High when the write port for tile RAM is enabled.
    );


    ////////////////////////
    // 2K of ROM.
    wire rom_write_enable;
    wire rom_chip_select;
    wire [7:0] rom_data;
    rom_1k rom(
      .clock(CLK),                  // Synchronous read clock.
      .addr(address_bus[10:0]),     // 11-bit ROM address to read for a 2KB area.
      .cs(rom_chip_select),         // High if the ROM is selected by the address bus.
      .data_bus_out(mux_input_rom)  // Output port to the data bus mux.
      );

    ////////////////////////
    // 1KB of RAM in 256-byte pages.
    wire work_ram_write_enable;
    wire [7:0] work_ram_data;
    ram_1k ram(
      .clock(CLK),                  // Synchronous access clock.
      .wen(cpu_write_line),         // Writing to RAM when this is high.
      .addr(address_bus[9:0]),      // 10-bit RAM address to read or write.
      .cs(ram_chip_select),         // High if the RAM chip is selected.
      .data_bus_in(data_from_cpu),  // Input port from the CPU.
      .data_bus_out(mux_input_ram)  // Output port to the data bus mux.
      );

    /* Assert the proper chip select wire when the address bus is in a certain range.. */
    assign rom_chip_select = `ADDRESS_RANGE(16'hF000, 16'hF7FF);
    assign ram_chip_select = `ADDRESS_RANGE(16'h0000, 16'h03FF);

    assign display_chip_select = `ADDRESS_RANGE(16'h9000, 16'h9001);
    assign uart_chip_select = `ADDRESS_RANGE(16'hA000, 16'hA00F);
    assign tilemap_chip_select = `ADDRESS_RANGE(16'h8000, 16'h8FFF);

endmodule
