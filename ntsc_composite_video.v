// 8MHz pixel clock

module ntsc_composite_video (input clock,
  output ntsc_signal,
  output ntsc_sync,
  output reg [9:0] xpos,  // horizontal clocks
  output reg [8:0] ypos,  // current scanline
  input draw_pixel);

//
// Define the beginning of HSYNC (in clocks) and VSYNC (in scanlines).
parameter HSYNC_START = 450;
parameter VSYNC_START = 240;

// Are we in a visible area?
wire active = xpos < 420 & ypos < VSYNC_START - 8;

// Are we in a sync area?
wire hsync = HSYNC_START <= xpos && xpos < HSYNC_START + 60;
wire vsync = VSYNC_START <= ypos && ypos < VSYNC_START + 3;

reg video_out;

assign ntsc_signal = active && draw_pixel;
assign ntsc_sync = active || !(hsync || vsync);

reg divide_by_two = 0;
wire clock_divided_by_two = divide_by_two;

always @(posedge clock) begin
  divide_by_two = divide_by_two + 1;
end

initial xpos = 0;
initial ypos = 0;

always @(posedge clock_divided_by_two)
begin
  if (xpos == HSYNC_START+80) begin
  xpos <= 0;
    if (ypos == VSYNC_START+35)
        ypos <= 0;
    else
        ypos <= ypos + 1;
    end
  else
    xpos <= xpos + 1;
end

endmodule
