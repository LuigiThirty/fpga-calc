// 4-character 7-segment LED driver.

module led_driver (
  // Digit advance
  input ADVANCE,

  // Four digit registers
  input [7:0] OUTPUT_DIGIT_0,
  input [7:0] OUTPUT_DIGIT_1,
  input [7:0] OUTPUT_DIGIT_2,
  input [7:0] OUTPUT_DIGIT_3,

  //Output latch. This value gets output on the GPIO pins.
  output reg [7:0] DISPLAY_LATCH,
  output reg [1:0] BLANKING
);

reg [1:0] selected_digit;

always @(posedge ADVANCE) begin
  selected_digit = selected_digit + 1;

  case (selected_digit)
    0: begin DISPLAY_LATCH <= ~OUTPUT_DIGIT_0; BLANKING <= 0; end
    1: begin DISPLAY_LATCH <= ~OUTPUT_DIGIT_1; BLANKING <= 1; end
    2: begin DISPLAY_LATCH <= ~OUTPUT_DIGIT_2; BLANKING <= 2; end
    3: begin DISPLAY_LATCH <= ~OUTPUT_DIGIT_3; BLANKING <= 3; end
  endcase
end

endmodule
