`ifndef FOUR_TO_ONE_MUX_V
`define FOUR_TO_ONE_MUX_V

module four_to_one_mux #(parameter BUS_WIDTH = 16)
(
    input [BUS_WIDTH-1:0] a, b, c, d,
    output [BUS_WIDTH-1:0] out,
    input [1:0] select
);

assign out = (select == 0) ? a : (select == 1) ? b : (select == 2) ? c : d;

endmodule

`endif
