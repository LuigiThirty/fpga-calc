module rom_1k (
  input clock,
  input [10:0] addr,

  output [7:0] data_bus_out,

  input cs
);

reg [10:0] mem[0:2047];

initial begin
  $readmemh("/home/luigi/PycharmProjects/canis-assembler/program.vmem", mem);
end

assign data_bus_out = mem[addr[10:0]];

endmodule
