task instruction_ld_axy(
  input reg [1:0] destination_register,
  input reg [7:0] operand,
  output [7:0] A, X, Y
  );

  case (destination_register)
    0: A = operand;
    1: X = operand;
    3: Y = operand;
    default: ;
  endcase

endtask
