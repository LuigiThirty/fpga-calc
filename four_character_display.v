module four_character_display
(
  input clock,
  output [15:0] address_bus,
  input [7:0] data_bus_in,

  input chip_select,

  output a,
  output b,
  output c,
  output d,
  output e,
  output f,
  output g,

  output digit0,
  output digit1,
  output digit2,
  output digit3
);

reg [15:0] number_to_display;

reg led_advance;
wire [7:0] led_output_latch;
wire [1:0] led_blanking;

// Four registers containing the LED segments given to the driver.
wire [7:0] digit0_segments;
wire [7:0] digit1_segments;
wire [7:0] digit2_segments;
wire [7:0] digit3_segments;

// 7 segment LED output pins
assign a = led_output_latch[0];
assign b = led_output_latch[1];
assign c = led_output_latch[2];
assign d = led_output_latch[3];
assign e = led_output_latch[4];
assign f = led_output_latch[5];
assign g = led_output_latch[6];

// LED blanking circuit. Only one digit can be selected at a time.
// The digit to display corresponds to the 4-byte LED output register.
// When the blanking bit changes, the output latch is updated.
assign digit0 = led_blanking == 0;
assign digit1 = led_blanking == 1;
assign digit2 = led_blanking == 2;
assign digit3 = led_blanking == 3;

led_driver led_driver (
  .ADVANCE(led_advance),
  .DISPLAY_LATCH(led_output_latch),
  .BLANKING(led_blanking),
  .OUTPUT_DIGIT_0(digit0_segments),
  .OUTPUT_DIGIT_1(digit1_segments),
  .OUTPUT_DIGIT_2(digit2_segments),
  .OUTPUT_DIGIT_3(digit3_segments)
  );

led_hex_decoder led_bcd_digit0 (
  .hex_in(number_to_display[15:12]),
  .segments_out(digit0_segments)
  );

led_hex_decoder led_bcd_digit1 (
  .hex_in(number_to_display[11:8]),
  .segments_out(digit1_segments)
);

led_hex_decoder led_bcd_digit2 (
  .hex_in(number_to_display[7:4]),
  .segments_out(digit2_segments)
);

led_hex_decoder led_bcd_digit3 (
  .hex_in(number_to_display[3:0]),
  .segments_out(digit3_segments)
);

reg [15:0] clock_counter;

always @(posedge clock) begin
  clock_counter = clock_counter + 1;

  if (clock_counter == 50000)
    begin
      led_advance = ~led_advance;
      clock_counter = 0;
    end

  if(chip_select)
    if(!address_bus[0])
      number_to_display[15:8] = data_bus_in[7:0];
    else
      number_to_display[7:0] = data_bus_in[7:0];
end

endmodule
