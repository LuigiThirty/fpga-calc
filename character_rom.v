module character_rom(
  input clock,
  input wen,
  input [10:0] addr,
  input [7:0] wdata,
  output reg [7:0] rdata
);

  reg [10:0] mem[0:1023];

  initial begin
    $readmemb("characterset.mem", mem);
  end

  always @(posedge clock) begin
    // wdata is not hooked up
    rdata = mem[addr];
  end

endmodule
