module canis_tb;

reg CLK = 0;

wire [9:0] VID_XPOS;
wire [8:0] VID_YPOS;

wire [15:0] address_bus;

wire display_chip_select;
wire tilemap_chip_select;
wire work_ram0_chip_select, work_ram1_chip_select, work_ram2_chip_select, work_ram3_chip_select;

// Video module
ntsc_composite_video ntsc_video_out(
  .clock(CLK),
  .ntsc_signal(PIN_9),
  .ntsc_sync(PIN_10),
  .xpos(VID_XPOS),
  .ypos(VID_YPOS),
  .draw_pixel(draw_pixel)
  );

  /* MUX */
  wire [7:0] mux_input_cpu, mux_input_ram, mux_input_tile_ram, mux_input_rom, mux_output;
  reg [7:0] mux_input_uart;
  wire [7:0] data_from_cpu;

  wire [2:0] mux_selector;
  eight_to_one_mux #(.BUS_WIDTH(8)) data_bus_mux(
    .a(mux_input_cpu),
    .b(mux_input_ram),
    .c(mux_input_tile_ram),
    .d(mux_input_rom),
    .e(mux_input_uart),
    .out(mux_output),
    .select(mux_selector)
  );

  assign mux_selector =
    uart_chip_select ? 4 :
    rom_chip_select ? 3 :
    tilemap_chip_select ? 2 :
    ram_chip_select ? 1 :
    0;

  /* UART */
  wire uart_chip_select;
  reg [7:0] uart_tx_byte = "A";
  reg uart_tx_ready = 0;
  wire uart_rst = 0;
  wire uart_tx_done;
  uart_out uart_transmit(
    .data_in(uart_tx_byte),
    .ready(uart_tx_ready),
    .rst(uart_rst),
    .tx(PIN_12),
    .tx_done(uart_tx_done),
    .clk(CLK)
    );

  reg uart_rx_did_read_last_byte;
  reg uart_clock_counter = 0;
  wire [7:0] fifo_byte_out;
  reg uart_fifo_pull_byte = 0;
  wire fifo_full, fifo_empty;
  wire [7:0] uart_rx_byte;
  uart_in uart_receive(
    .srl_in(PIN_13),
    .data_out(uart_rx_byte),
    .clk(CLK),
    .rst(uart_rst),
    .new_byte_ready(uart_rx_new_byte_ready),
    .read_new_byte(uart_rx_read_new_byte)
    );

  always @(posedge CLK)
  begin
    // UART address $0: read the last byte received, pulse the clear line
    if(address_bus[1:0] == 0 && uart_chip_select)
    begin
      if(uart_clock_counter == 0)
      begin
        uart_fifo_pull_byte = 1;
        mux_input_uart = fifo_byte_out;
        uart_clock_counter = 1;
      end
    end

    else if(address_bus[1:0] == 1 && uart_chip_select)
    begin
      mux_input_uart = (fifo_empty ? 8'h80 : 0) | (fifo_full ? 8'h40 : 0);
    end

    if(!uart_chip_select) begin uart_clock_counter = 0; uart_fifo_pull_byte = 0; end
  end

canis cpu (
  .clock(CLK),
  .address_bus(address_bus),    // 16-bit
  .data_bus_in(mux_output),     // 8-bit
  .data_bus_out(data_from_cpu), // 8-bit
  .rw(cpu_write_line)
);

// Tilemap stuff
wire [11:0] tilemap_address;
wire tilemap_write_en;
wire [7:0] tile_number;
wire tile_ram_chip_select;
wire [7:0] tilemap_data_out;

wire [11:0] video_fetch_address;
wire [7:0] video_fetch_out;

tilemap_ram tilemap_data(
  .clock(CLK),

  // from tilemap engine
  .wen(tilemap_write_en),
  .addr(tilemap_address),
  .wdata(tilemap_data_out),
  .rdata(tilemap_read_line),

  // Video port - dual-port memory
  .video_port_addr(video_fetch_address),
  .video_port_rdata(tile_number)
  );

tilemap_display tilemap(
  .clock(CLK),
  .xpos(VID_XPOS),
  .ypos(VID_YPOS),
  .pixel(tilemap_draw_pixel),

  .tile_number_in(tile_number),
  .tilemap_address_out(tilemap_address),
  .tilemap_data_out(tilemap_data_out),

  .video_address_out(video_fetch_address),

  .address_bus(address_bus),
  .data_bus_in(data_from_cpu),
  .cs(tilemap_chip_select),
  .rw(cpu_write_line),

  .tilemap_write_en(tilemap_write_en)
  );

  // ROM
  wire rom_write_enable;
  wire rom_chip_select;
  wire [7:0] rom_data;
  rom_1k rom(
    .clock(CLK),
    .addr(address_bus[10:0]),
    .cs(rom_chip_select),
    .data_bus_out(mux_input_rom)
    );

  // RAM modules
  wire work_ram_write_enable;
  wire [7:0] work_ram_data;
  ram_1k ram(
    .clock(CLK),
    .wen(cpu_write_line),
    .addr(address_bus[9:0]),
    .cs(ram_chip_select),
    .data_bus_in(data_from_cpu),
    .data_bus_out(mux_input_ram)
    );

  reg [7:0] fifo_debug_data = 0;
  reg fifo_push = 0;
  reg fifo_pull = 0;
  wire [7:0] fifo_output;

  fifo_8 uart_fifo(
    .r_clock(CLK),
    .w_clock(~CLK),

    .in_data(fifo_debug_data),
    .push_byte(fifo_push),

    .pull_byte(uart_fifo_pull_byte),

    //.in_data(uart_rx_byte),
    //.push_byte(uart_rx_new_byte_ready),

    .out_data(fifo_byte_out),

    .full(fifo_full),
    .empty(fifo_empty)
    );

  /* Assert the proper chip select wire. */
  assign rom_chip_select = `ADDRESS_RANGE(16'hF000, 16'hF7FF);
  assign ram_chip_select = `ADDRESS_RANGE(16'h0000, 16'h03FF);

  assign display_chip_select = `ADDRESS_RANGE(16'h9010, 16'h9011);
  assign uart_chip_select = `ADDRESS_RANGE(16'hA000, 16'hA00F);
  assign tilemap_chip_select = `ADDRESS_RANGE(16'h8000, 16'h8FFF);

always #1 CLK = ~CLK;

initial begin
  $dumpfile ("canis_tb.vcd");
  $dumpvars;
end

initial begin
  #2 fifo_debug_data = 8'h41;
  #3 fifo_push = 1;
  #4 fifo_push = 0;

  #20000 $finish;
end

endmodule
