// A tilemapped display.

module tilemap_display
(
      input clock,
      input [9:0] xpos,
      input [8:0] ypos,
      output reg pixel,

      input [7:0] tile_number_in,
      output reg [11:0] tilemap_address_out,
      output reg [7:0] tilemap_data_out,

      input [15:0] address_bus,
      input [7:0] data_bus_in,
      input cs,
      input rw,

      output reg tilemap_write_en,

      output reg [11:0] video_address_out
);

// Video parameters.
parameter LEFT_BORDER = 50;
parameter RIGHT_BORDER = 379;
parameter TOP_BORDER = 5;
parameter BOTTOM_BORDER = 205;

wire [7:0] chardata_out;      // Routes character data to the tile engine.
reg [10:0] character_address;  // Stores the character address for reading.

wire write_en;                // Dummy, always 0.
wire [7:0] write_data;        // Dummy, always 0.
assign write_en = 0;
assign write_data = 8'h0;

// Character ROM.
character_rom char_data(
  .clock(clock),              // Clock
  .wen(write_en),             // ROM write enable, always 0
  .addr(character_address),   // Address of the character line we want to read`
  .wdata(write_data),         // ROM write data, always 0
  .rdata(chardata_out)        // Character data out, one byte of 1bpp graphics
  );

// Keep track of X and Y offset inside a tile.
reg [4:0] tile_x_offset;
reg [4:0] tile_y_offset;

// Tile index, updated when the current beam position passes to another tile.
reg [8:0] tile_x_index;
reg [8:0] tile_y_index;

reg [9:0] old_tile_x; // Keep track of the tile we drew on the previous video clock.

//
// Tile update registers...
// $8000 - update_tilemap_address lo
// $8001 - update_tilemap_address hi
// $8002 - update_tilemap_data
reg [15:0] update_tilemap_address;
reg [7:0] update_tilemap_data;

// Don't write until tilemap_write_en has been toggled.
reg tilemap_write_lockout = 0;

reg trigger_tilemap_write;

always @(negedge clock) begin
  if(trigger_tilemap_write)
    tilemap_write_en = 1;
  else
    tilemap_write_en = 0;
end

//
always @(posedge clock) begin

  // Update tile data if needed.
  if(!tilemap_write_en) tilemap_write_lockout = 0;
  else if(trigger_tilemap_write) begin trigger_tilemap_write = 0; update_tilemap_address = update_tilemap_address + 1; end

  if(cs)
  begin
    if(address_bus[1:0] == 0)
      begin update_tilemap_address[7:0] <= data_bus_in; end
    else if(address_bus[1:0] == 1)
      begin update_tilemap_address[15:8] <= data_bus_in; end
    else if(address_bus[1:0] == 2 && rw)
    begin
      tilemap_address_out = update_tilemap_address;
      tilemap_data_out = data_bus_in;
      trigger_tilemap_write = 1;
    end
  end

  // Always update these registers.
  tile_x_offset <= (xpos - 1 - LEFT_BORDER) % 8;
  tile_y_offset <= (ypos - TOP_BORDER) % 8;

  // The (X,Y) index of the tile on the display.
  tile_x_index <= ((xpos - LEFT_BORDER) / 8);
  tile_y_index <= (ypos - TOP_BORDER) / 8;

  // Are we on a new X pos?
  if(tile_x_index != old_tile_x)
    begin
      old_tile_x <= tile_x_index; // Update the old tile register

      // Fetch the character from tile RAM and the graphics from char ROM.
      video_address_out = (tile_y_index * 40) + tile_x_index;
      character_address = (tile_number_in << 3) + tile_y_offset;
    end

  // Draw if we are within the visible area.
  if(xpos > LEFT_BORDER && xpos < RIGHT_BORDER && ypos > TOP_BORDER && ypos < BOTTOM_BORDER)
    pixel <= chardata_out[7-tile_x_offset]; // otherwise the tiles are horizontally flipped
  else
    pixel <= 0;
end

endmodule
