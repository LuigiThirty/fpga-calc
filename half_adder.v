module half_adder(a, b, c, s);

input a;
input b;
output c; // SUM
output s; // CARRY

assign c = a & b;
assign s = a ^ b;

endmodule
